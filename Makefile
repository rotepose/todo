# directories
INCLUDE = include
OBJECTS = objects
SRC = src

# flags, et. al.
CC = gcc
CFLAGS = -I$(INCLUDE) -g -Wall -Wextra -Wstrict-prototypes \
		 -Wmissing-prototypes -Wshadow -Wconversion -fanalyzer
LIBS = -lsqlite3 #-ncurses

# files of concern
_DEPS = todo.h sql_crud.h #gui_todo.h
DEPS = $(patsubst %,$(INCLUDE)/%,$(_DEPS))

_OBJ = todo.o sql_crud.o #gui_todo.o
OBJ = $(patsubst %,$(OBJECTS)/%,$(_OBJ))

# what to do with them
$(OBJECTS)/%.o: $(SRC)/%.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

todo : $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)

# remove files
# .PHONY : clean <- I wonder what the story is behind this...
clean :
	rm $(OBJECTS)/*.o todo
