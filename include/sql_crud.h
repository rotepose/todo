#ifndef SQL_CRUD_H
#define SQL_CRUD_H

// includes
#include <sqlite3.h> // for the database
#include <stdio.h> // for commandline
#include <stdlib.h> // dynamic memory
#include <sys/stat.h> // for exists function
#include <stdarg.h> // variadic func
#include <string.h> // memcpy
#include <unistd.h> // getopt_long, EXIT_FAILURE, EXIT_SUCCESS
#include <ncurses.h>

// defines
#define DB_NAME ".todo.db"

struct tasks {
	int pri;
	char *task;
};

// declarations
int db_init(void);
int exists(const char*);
void init_check(void);
int db_add(char *, char *, const char *);
char * vari_cat(char *, ...);
char * get_db_path(void);
int db_list(void);
int db_pri_set(int, int);
void db_close(void);
int db_complete(int);
int db_remove(int);
int list_count(void);
void db_gui_list(struct tasks *);

#endif
