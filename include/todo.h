#ifndef TODO_H
#define TODO_H

/* system includes */
#include <unistd.h> // getopt_long, EXIT_FAILURE, EXIT_SUCCESS
#include <stdio.h> // commandline
#include <stdlib.h> // getenv, et. al.
#include <getopt.h> // argc, argv

/* defines */
#define PACKAGE_BUGREPORT "bugs@rotepose.com"
#define PACKAGE_NAME "todo"
#define PACKAGE_URL "https://gitlab.com/rotepose23/todo/"
#define OPSTR "hva:c:r:l"
#define TASK_MASTER "task_master"
#define TASK_MSG "task_msg"

/* global */
const char *program_name;

/* function declarations */
void print_help(void);
void print_version(void);

#endif
