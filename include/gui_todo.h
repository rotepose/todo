/*
|
|  Copyright: Jeremy Cartwright (C) 2021
|    License: BSD 3 Clause License
|
|     Author: Jeremy Cartwright
|    Contact: jeremy@rotepose.com
|    Created: 16-MAR-2021
| gui_todo.h: header file for todo ncurses
|
*/
#ifndef GUI_TODO_H
#define GUI_TODO_H

/*  00 System Includes */
#include <ncurses.h>
#include <ctype.h>

/*  01 Project Includes */
#include "sql_crud.h"

/*  02 Externs */

/*  03 Defines */
#define HEIGHT LINES
#define t_WIDTH() (int)(COLS * .8)
#define m_WIDTH() (int)(COLS * .2)
#define N_CHOICES 7
#define ADD 1
#define COMPLETE 2
#define REMOVE 3
#define LIST 4
#define HELP 5
#define VERSION 6
#define EXIT 7

/*  04 Typedefs */
	typedef struct window_type {
		WINDOW *menu;
		WINDOW *body;
	} window;

/*  05 Globals */

/*  06 Function Declarations */
int gui_init(void);
void gui_list(window);
void gui_menu(window);
void choice_menu(window, int);
void gui_add(window);
void select_menu(window, int);
void gui_home(window);

#endif
