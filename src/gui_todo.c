/*
|
| Copyright: Jeremy Cartwright (C) 2021
|   License: BSD 3 Clause License
|
|    Author: Jeremy Cartwright
|   Contact: jeremy@rotepose.com
|   Created: 16-MAR-2021
|  gui_todo: todo ncurses gui functions
|
*/
#include "gui_todo.h"

// setup ncurses environment
int
gui_init(void) {
/*{{{*/
	int startx, starty;
	static window panes;

	// init ncurses environment
	initscr();
	clear();
	noecho();
	cbreak();
	curs_set(0);
	start_color();

	// setup of screen, gui
	startx = 0;
	starty = 0;
	panes.menu = newwin(HEIGHT, m_WIDTH(), starty, startx);
	keypad(panes.menu, TRUE);
	
	startx = m_WIDTH();
	starty = 0;
	panes.body = newwin(HEIGHT, t_WIDTH(), starty, startx);
	keypad(panes.body, TRUE);

	// print list & menu
	gui_home(panes);

	// End curses mode
	endwin();

	return 0;

}/*}}}*/

// print list
void
gui_list(window panes) {
/*{{{*/
	int i = list_count(), x = 2, y = 3;
	struct tasks list[i];
	struct tasks *list_p = list;

	// create line across top
	mvwprintw(panes.body, 2, 2, "%s", "todo");
	mvwhline(panes.body, y, x, ACS_HLINE, t_WIDTH() - (x * 2));

	// populate struct []
	db_gui_list(list_p);

	x = 0, y = 4;
	// print list
	while (x < i) {
		mvwprintw(panes.body, y, 2, "%i : %s\n", list[x].pri, list[x].task);
		x++, y++; 
	}
	wrefresh(panes.body);

	// free memory of task string
	for (x = 0; x < i; x++) {
		free(list[x].task);
	}

	return;

}/*}}}*/

// create interface for adding task
void
gui_add(window panes) {
/*{{{*/

	int i, x = 2, y = 2;

	wclear(panes.body);

	// create line across top
	mvwprintw(panes.body, y++, x, "%s", "todo");
	mvwhline(panes.body, y++, x, ACS_HLINE, t_WIDTH() - (x * 2));
	mvwprintw(panes.body, y++, x, "%s", "what do you need todo:");
	mvwhline(panes.body, y--, x, ACS_HLINE, t_WIDTH() - (x * 2));


	while (1) {
		if ( (i = wgetch(panes.body)) == 'q') {
			wclear(panes.body);
			gui_home(panes);
			return;
		}
	}

}/*}}}*/

// coordinate menu
void
gui_menu(window panes) {
/*{{{*/

	int c;
	int highlight = 1;
	int choice = 0;

	// print initial menu
	choice_menu(panes, highlight);

	// navigate menu with highlighting
	while(1) {
		c = wgetch(panes.menu);
		switch(c) {
			case KEY_UP:
				if ( highlight == 1)
					highlight = N_CHOICES;
				else
					--highlight;
				break;
			case 'k':
				if ( highlight == 1)
					highlight = N_CHOICES;
				else
					--highlight;
				break;
			case KEY_DOWN:
				if ( highlight == N_CHOICES)
					highlight = 1;
				else
					++highlight;
				break;
			case 'j':
				if ( highlight == N_CHOICES)
					highlight = 1;
				else
					++highlight;
				break;
			case 10:
				choice = highlight;
				break;
			default:
				mvwprintw(panes.menu, HEIGHT - 2, 0, "'%c'", c);
				wrefresh(panes.menu);
				break;
		}
		choice_menu(panes, highlight);

		// break loop on choice
		if ( choice != 0 )
			break;
	}

	// deal with choice
	// break this out and get choice_menu in
	switch (choice) {
		case ADD:
			// add function here
			select_menu(panes, highlight);
			gui_add(panes);
			break;
		case COMPLETE:
			// complete function here
			break;
		case REMOVE:
			// add function here
			break;
		case LIST:
			gui_home(panes);
			break;
		case HELP:
			// add function here
			break;
		case VERSION:
			// add function here
			break;
		case EXIT:
			return;
			break;
		default:
			db_close();
			exit (EXIT_FAILURE);
	}

	endwin();
	db_close();
	exit (EXIT_FAILURE);

}/*}}}*/

// selected menu
void
choice_menu(window panes, int highlight) {
/*{{{*/
	int x, y, i;
	char *choices[] = {
		"     Add",
		"Complete",
		"  Remove",
		"    List",
		"    Help",
		" Version",
		"    Exit",
	};

	x = 6;
	y = 2;
	wborder(panes.menu, 1, 0, 1, 1, 1, 1, 1, 1);

	for (i = 0; i < N_CHOICES; ++i) {
		// highlight the present choice
		if ( highlight == i + 1 ) {
			wattron(panes.menu, A_REVERSE);
			mvwprintw(panes.menu, y, x, "%s", choices[i]);
			wattroff(panes.menu, A_REVERSE);
		} else {
			wattron(panes.menu, A_DIM);
			mvwprintw(panes.menu, y, x, "%s", choices[i]);
			wattroff(panes.menu, A_DIM);
		}
		++y;
	}
	wrefresh(panes.menu);

	return;

}/*}}}*/

// print selected choice on menu screen 
// : hvacrl to be on par with current functionality
void
select_menu(window panes, int highlight) {
/*{{{*/
	int x, y, i;
	char *choices[] = {
		"     Add",
		"Complete",
		"  Remove",
		"    List",
		"    Help",
		" Version",
		"    Exit",
	};

	wclear(panes.menu);

	x = 6;
	y = 2;
	wborder(panes.menu, 1, 0, 1, 1, 1, 1, 1, 1);

	for (i = 0; i < N_CHOICES; ++i) {
		// highlight the present choice
		if ( highlight == i + 1 ) {
			mvwprintw(panes.menu, y, x, "%s", choices[i]);
		} else {
			mvwprintw(panes.menu, y, x, "%s", "");
		}
		++y;
	}
	wrefresh(panes.menu);

	return;

}/*}}}*/

// wrapper for gui reset
void
gui_home(window panes) {
/*{{{*/

	// clear and print list
	wclear(panes.body);
	gui_list(panes);

	// clear and print menu
	wclear(panes.menu);
	gui_menu(panes);

}/*}}}*/
