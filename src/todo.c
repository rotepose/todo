/*
|
| Copyright: Jeremy Cartwright (C) 2021
|   License: BSD 3 Clause License
|
|    Author: Jeremy Cartwright
|   Contact: jeremy@rotepose.com
|   Created: 24-FEB-2021
|      todo: a to do list with an eye toward ncurses and dayplanning
|			 (cal, schedule, WebDAV?, bible verse of the day?...)
|            
*/

/* Project Includes */
#include "todo.h"
#include "sql_crud.h"
//#include "gui_todo.h"

// TODO get a lot of this into boilerplate
int
main(int argc, char **argv) {
/*{{{*/

    /* Variable Declarations */
    // missing long-range-task, subtasks, recurring tasks,
    // priority, percentage-done, and task-families
	int optc;
	int l = 0, lose = 0;
	const char *add = NULL;
	int remove = 0;
	int complete = 0;
    static const struct option longopts[] =
    {
        { "help", no_argument, NULL, 'h' },
        { "version", no_argument, NULL, 'v' },
        { "add", required_argument, NULL, 'a' },
        { "complete", required_argument, NULL, 'c' },
        { "remove", required_argument, NULL, 'r' },
 //       { "list", no_argument, NULL, 'l' },
        { NULL, 0, NULL, 0 }
    };

    /* Check argv[0] to see how the program was invoked */
    program_name = argv[0];
    
    /* Process the Command Line Options from the user */
    // TODO these options need to be expanded to take advantage of the
    // functionality built into the database? or is that only through
    // the gui? also, l should have :: so it can list long-range-tasks and
    // their subtasks only?
	while ((optc = getopt_long (argc, argv, OPSTR, longopts, NULL)) != -1)
		switch (optc)
		{
			case 'h':
				print_help();
				exit (EXIT_SUCCESS);
				break;
			case 'v':
				print_version();
				exit (EXIT_SUCCESS);
				break;
			case 'a':
				add = optarg;
				break;
			case 'c':
                // TODO add if statement to check for proper index number
				// given, as well as a confirmation of completion
				complete = atoi(optarg);
				break;
			case 'r':
                // TODO add if statement to check for proper index number
				// given, as well as a confirmation of completion
				remove = atoi(optarg);
				break;
//			case 'l':
//				l = 1;
//				break;
			default:
				lose = 1;
				break;
		}

	if (lose || optind < argc)
	{
		/* Print error message and exit. */
		if (optind < argc)
			fprintf (stderr, ("%s: extra operand: %s\n"),
					program_name, argv[optind]);
		fprintf (stderr, ("Try `%s --help' for more information.\n"),
				program_name);
		exit (EXIT_FAILURE);
    }

    /* options processed, on to code */
	
    // if ( argc == 1) { launch ncurses...
    init_check();

	if ( argc == 1 ) {
		//gui_init();
		db_list();
	} else if ( add ) {
		db_add(TASK_MASTER, TASK_MSG, add);
		db_pri_set(-1, -1);
		db_list();
//	} else if ( l == 1 ) {
//		db_list();
	} else if ( complete ) {
		db_complete(complete);
		db_list();
	} else if ( remove ) {
		db_remove(remove);
		db_list();
	}

	db_close();
	exit (EXIT_SUCCESS);

}/*}}}*/

// print help
void
print_help(void) {
/*{{{*/

  printf ("Usage: %s [OPTION]...\n", program_name);
  printf ("Keep track of planned tasks.\n\n");
  printf ("\t-h, --help\t\tdisplay this help and exit\n\t-v, --version\t\tdisplay version information and exit\n\n");
  printf ("\t-a, --add=\"TEXT\"\tadds TEXT as a list item\n\t-c, --complete=INDEX\tcompletes INDEX list item\n\t-r, --remove=INDEX\tremoves INDEX list item\n\t-l, --list\t\tprints your todo list\n\n");

  return;

}/*}}}*/

// print version number
void
print_version(void) {
/*{{{*/

		printf("VerSION 0.011\n");

	return;

}/*}}}*/
