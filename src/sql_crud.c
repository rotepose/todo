/*
|
| Copyright: Jeremy Cartwright (C) 2021
|   License: BSD 3 Clause License
|
|    Author: Jeremy Cartwright
|   Contact: jeremy@rotepose.com
|   Created: 08-MAR-2021
|  sql_crud: funcs for working with sqlite3
|
*/

#include "sql_crud.h"

// globals
sqlite3 *db;

// ensure database is setup properly
void
init_check(void) {
/*{{{*/
	char *path = get_db_path();

    if ( !exists(path) ) {
        db_init();
	} else {
		int rc;

		rc = sqlite3_open_v2(path, &db, SQLITE_OPEN_READWRITE, NULL);
		if ( rc ) {
			fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
			sqlite3_close(db);
			exit (EXIT_FAILURE);
		}
	}

	free(path);

	return;

}/*}}}*/

// check for file existence
int
exists(const char *path) {
/*{{{*/

    struct stat buf;
    return (stat(path, &buf) == 0);

}/*}}}*/

// initialize database
int
db_init(void) {
/*{{{*/

	char *sql_err = 0;
	int rc;
    char *sql;
	char *path = get_db_path();

	rc = sqlite3_open_v2(path, &db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL);

	if ( rc ) {
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
		free(path);
		db_close();
		exit (EXIT_FAILURE);
	} else {
		fprintf(stderr, "Opened database successfully\n");
	}

    sql = "CREATE TABLE IF NOT EXISTS task_master(" \
        "task_id INTEGER PRIMARY KEY NOT NULL UNIQUE DEFAULT (strftime('%s', 'now'))," \
        "task_priority INT NOT NULL DEFAULT (strftime('%s', 'now'))," \
        "task_msg TEXT NOT NULL DEFAULT 'eat fruits and veggies'," \
        "task_progress INT NOT NULL DEFAULT 0," \
        "task_done INT NOT NULL DEFAULT 0) WITHOUT ROWID;";

    rc = sqlite3_exec(db, sql, NULL, NULL, &sql_err);

    if ( rc ) {
        fprintf(stderr, "SQL error: %s\n", sql_err);
        sqlite3_free(sql_err);
    } else {
        fprintf(stdout, "Table task_master created successfully.\n");
    }

    sql = "CREATE TABLE IF NOT EXISTS task_family(" \
        "task_id INTEGER PRIMARY KEY NOT NULL UNIQUE," \
        "task_fam TEXT[20] NOT NULL DEFAULT 'todo') WITHOUT ROWID;";

    rc = sqlite3_exec(db, sql, NULL, NULL, &sql_err);

    if ( rc ) {
        fprintf(stderr, "SQL error: %s\n", sql_err);
        sqlite3_free(sql_err);
    } else {
        fprintf(stdout, "Table task_family created successfully.\n");
    }

    sql = "CREATE TABLE IF NOT EXISTS task_tree(" \
        "task_id INTEGER PRIMARY KEY NOT NULL UNIQUE," \
        "is_parent INT NOT NULL DEFAULT 0," \
        "is_child INT NOT NULL DEFAULT 0," \
        "parent_id INT REFERENCES task_master(task_id)," \
        "child_id INT REFERENCES task_master(task_id)," \
        "CHECK (is_parent <> 1 OR parent_id is NOT NULL)," \
        "CHECK (is_child <> 1 OR child_id is NOT NULL)) WITHOUT ROWID;";

    rc = sqlite3_exec(db, sql, NULL, NULL, &sql_err);

    if ( rc ) {
        fprintf(stderr, "SQL error: %s\n", sql_err);
        sqlite3_free(sql_err);
    } else {
        fprintf(stdout, "Table task_tree created successfully.\n");
    }

    sql = "CREATE TABLE IF NOT EXISTS task_freq(" \
        "task_id INTEGER PRIMARY KEY NOT NULL UNIQUE," \
        "frequency TEXT NOT NULL DEFAULT 'weekly'," \
        "date_start INT NOT NULL DEFAULT (strftime('%s', 'now'))," \
        "date_stop INT NOT NULL DEFAULT 0," \
        "lead_time INT NOT NULL DEFAULT 3) WITHOUT ROWID;";

    rc = sqlite3_exec(db, sql, NULL, NULL, &sql_err);

    if ( rc ) {
        fprintf(stderr, "SQL error: %s\n", sql_err);
        sqlite3_free(sql_err);
		db_close();
		exit (EXIT_FAILURE);
    } else {
        fprintf(stdout, "Table task_freq created successfully.\n");
    }

	free(path);
	printf("\nWelcome to todo. Call todo -h for available actions.\n");

    exit (EXIT_SUCCESS);

}/*}}}*/

// get database path - likely going to define this later
char *
get_db_path(void) {
/*{{{*/

    char *home = getenv("HOME");
    char *db_path = vari_cat(home, "/", DB_NAME, NULL);

	return db_path;

}/*}}}*/

// add data to database TODO make parameterized
int
db_add(char *table, char *column, const char *value) {
/*{{{*/

	char *sql_err = 0;
	int rc;
    char *sql;

	// set sql command
	// TODO parameterize this; requires refactoring to stmt prep
	sql = vari_cat("INSERT INTO ", table, "(", column, ")" \
		   "VALUES ('", value, "');", NULL);
	// enter values into database
	rc = sqlite3_exec(db, sql, NULL, NULL, &sql_err);

	if ( rc ) {
		fprintf(stderr, "SQL error: %s\n", sql_err);
		sqlite3_free(sql_err);
		db_close();
		exit (EXIT_FAILURE);
	}

	return 0;

}/*}}}*/

// list todo items.
// TODO perhaps add vari ability to list families
int
db_list(void) {
/*{{{*/

	char *sql_err = 0;
	sqlite3_stmt *stmt;
	int rc;
    char *sql;

	// set sql command
	sql = "SELECT task_msg, task_priority FROM task_master WHERE "\
		   "task_done = 0 ORDER BY task_priority;";

	// prepare select statement
	rc = sqlite3_prepare_v2(db, sql, -1, &stmt, 0);

	if ( rc ) {
		fprintf(stderr, "SQL error: %s\n", sql_err);
		sqlite3_free(sql_err);
		db_close();
		exit (EXIT_FAILURE);
	} 

	// execute statement
	while ( sqlite3_step(stmt) == 100 ) {
		printf("%2i : %s\n", sqlite3_column_int(stmt, 1),\
					sqlite3_column_text(stmt, 0));
	}

	sqlite3_finalize(stmt);

	return 0;

}/*}}}*/
// populate struct for gui to print list
//void
//db_gui_list(struct tasks *t) {
/*{{{*/ /*
	char *sql_err = 0;
	sqlite3_stmt *stmt;
	int rc;
    char *sql;

	// set sql command
	sql = "SELECT task_msg, task_priority FROM task_master WHERE "\
		   "task_done = 0 ORDER BY task_priority;";

	// prepare select statement
	rc = sqlite3_prepare_v2(db, sql, -1, &stmt, 0);

	if ( rc ) {
		fprintf(stderr, "SQL error: %s\n", sql_err);
		sqlite3_free(sql_err);
		db_close();
		exit (EXIT_FAILURE);
	} 

	// pass to caller:
	while ( sqlite3_step(stmt) == 100 ) {
		t->pri = sqlite3_column_int(stmt, 1);
		t->task = (char *) malloc(sqlite3_column_bytes(stmt, 0));
		if (t->task == NULL) {
			free(t->task);
			return;
		}
		strcpy(t->task, (char *)sqlite3_column_text(stmt, 0));
		++t;
	}

	return;

}*/
/*}}}*/

// confirm completion and mark complete tasks
int
db_complete(int pri) {
/*{{{*/
	char *sql_err = 0;
	sqlite3_stmt *stmt;
	char *sql;
	int d = 0, rc;
	char c;
    
	d = list_count();
	
	if (pri > d) {
		printf("Task %i is not valid.\n", pri);
	} else {
		// prepare statement and handle exception
		sql = "SELECT task_id, task_priority, task_msg FROM task_master "\
			   "WHERE task_priority = ?;";
		rc = sqlite3_prepare_v2(db, sql, -1, &stmt, 0);

		if ( rc ) {
			fprintf(stderr, "SQL error: %s\n", sql_err);
			sqlite3_free(sql_err);
			sqlite3_finalize(stmt);
			db_close();
			exit (EXIT_FAILURE);
		}
		sqlite3_bind_int(stmt, 1, pri);
		sqlite3_step(stmt);
		printf("You wish to mark this task complete?\n%2i : %s\n(y|n):> ",\
				sqlite3_column_int(stmt, 1), sqlite3_column_text(stmt, 2));

		// verify complete
		c = getchar();
		if ( c == 'Y' || c == 'y' ) {
			db_pri_set(pri, sqlite3_column_int(stmt, 0));
		}
	}

	return 0;

}/*}}}*/

// confirm deletion and remove task from db
int
db_remove(int pri) {
/*{{{*/
	char *sql_err = 0;
	sqlite3_stmt *stmt;
	char *sql;
	int d = 0, rc;
	char c;
    
	d = list_count();
	
	if (pri > d) {
		printf("Task %i is not valid.\n", pri);
	} else {
		// prepare statement and handle exception
		sql = "SELECT task_priority, task_msg FROM task_master "\
			   "WHERE task_priority = ?;";
		rc = sqlite3_prepare_v2(db, sql, -1, &stmt, 0);

		if ( rc ) {
			fprintf(stderr, "SQL error: %s\n", sql_err);
			sqlite3_free(sql_err);
			sqlite3_finalize(stmt);
			db_close();
			exit (EXIT_FAILURE);
		}
		sqlite3_bind_int(stmt, 1, pri);
		sqlite3_step(stmt);
		printf("You wish to permanently remove this task from database?"\
				"\n%2i : %s\n(y|n):> ", sqlite3_column_int(stmt, 0),\
				sqlite3_column_text(stmt, 1));

		// verify complete
		c = getchar();
		if ( c == 'Y' || c == 'y' ) {
			db_pri_set(pri, -1);
		}
	}

	return 0;

}/*}}}*/

// update todo list priority numbers
int
db_pri_set(int from, int to) {
/*{{{*/

	char *sql_err = 0;
	sqlite3_stmt *stmt;
	char *sql;
	int d = 0, rc, id, pri;

	d = list_count();

	// if passed from db_add, set last item to proper index
	if ( from == -1 && to == -1 ) {
		sql = "UPDATE task_master SET task_priority = ? WHERE task_priority"\
			   " = (SELECT max(task_priority) FROM task_master);";
		rc = sqlite3_prepare_v2(db, sql, -1, &stmt, 0);

		if ( rc ) {
			fprintf(stderr, "SQL error: %s\n", sql_err);
			sqlite3_free(sql_err);
			db_close();
			exit (EXIT_FAILURE);
		} else {
			sqlite3_bind_int(stmt, 1, d);
			sqlite3_step(stmt);
			sqlite3_finalize(stmt);
		}
		// if passed from db_complete, mark as complete and adjust list
	} else if ( to > d ) {
		// stage one: set task as completed
		sql = "UPDATE task_master SET task_priority = -1, task_progress = "\
			   "100, task_done = 1 WHERE task_id = @id;";
		rc = sqlite3_prepare_v2(db, sql, -1, &stmt, 0);

		if ( rc ) {
			fprintf(stderr, "SQL error: %s\n", sql_err);
			sqlite3_free(sql_err);
			db_close();
			exit (EXIT_FAILURE);
		}

		id = sqlite3_bind_parameter_index(stmt, "@id");
		sqlite3_bind_int(stmt, id, to);
		sqlite3_step(stmt);
		sqlite3_finalize(stmt);

		// stage two: move everything behind task up one
		sql = "UPDATE task_master SET task_priority = task_priority - 1 "\
			   "WHERE task_priority > @pri;";
		rc = sqlite3_prepare_v2(db, sql, -1, &stmt, 0);

		if ( rc ) {
			fprintf(stderr, "SQL error: %s\n", sql_err);
			sqlite3_free(sql_err);
			db_close();
			exit (EXIT_FAILURE);
		}

		pri = sqlite3_bind_parameter_index(stmt, "@pri");
		sqlite3_bind_int(stmt, pri, from);
		sqlite3_step(stmt);
		sqlite3_finalize(stmt);
		// if passed from db_remove, delete task from database
	} else if ( from != -1 && to == -1 ) {
		sql = "DELETE FROM task_master WHERE task_priority = @pri;";
		rc = sqlite3_prepare_v2(db, sql, -1, &stmt, 0);

		if ( rc ) {
			fprintf(stderr, "SQL error: %s\n", sql_err);
			sqlite3_free(sql_err);
			db_close();
			exit (EXIT_FAILURE);
		}

		pri = sqlite3_bind_parameter_index(stmt, "@pri");
		sqlite3_bind_int(stmt, pri, from);
		sqlite3_step(stmt);
		sql = "UPDATE task_master SET task_priority = task_priority - 1 "\
			   "WHERE task_priority > @pri;";
		rc = sqlite3_prepare_v2(db, sql, -1, &stmt, 0);

		if ( rc ) {
			fprintf(stderr, "SQL error: %s\n", sql_err);
			sqlite3_free(sql_err);
			db_close();
			exit (EXIT_FAILURE);
		}

		pri = sqlite3_bind_parameter_index(stmt, "@pri");
		sqlite3_bind_int(stmt, pri, from);
		sqlite3_step(stmt);
		sqlite3_finalize(stmt);
	}
		
	return 0;

}/*}}}*/

// returns count of list
int
list_count(void) {
/*{{{*/
	char *sql_err = 0;
	sqlite3_stmt *stmt;
	char *sql;
	int d = 0, rc;
    
	// prepare statement and handle exception
	sql = "SELECT * FROM task_master WHERE task_done = 0;";
	rc = sqlite3_prepare_v2(db, sql, -1, &stmt, 0);

	if ( rc ) {
		fprintf(stderr, "SQL error: %s\n", sql_err);
		sqlite3_free(sql_err);
		db_close();
		exit (EXIT_FAILURE);
	}

	// get count of list
	while ( sqlite3_step(stmt) == 100 ) {
		d++;
	}

	// we're done with the stmt
	sqlite3_finalize(stmt);

	return d;

}/*}}}*/

// terminate call with NULL, i.e. vari_cat(str1, str2, NULL);
// borrowed heavily from the libc manual
char *
vari_cat(char *str, ...) {
/*{{{*/

	va_list ap;
	size_t buf = 10;
	char *cat = (char*) calloc(buf, sizeof(*cat));

	if (cat != NULL) {
		char *dynamem;
		char *tempcat;
		const char *s;

		va_start(ap, str);

		tempcat = cat;
		for (s = str; s != NULL; s = va_arg(ap, char *)) {
			size_t len = strlen(s);

			// Resize the allocated memory if necessary
			if ((tempcat + len + 1) > (cat)) {
				buf = (buf + len) * 2;
				dynamem = (char *) realloc(cat, buf);
				if (dynamem == NULL) {
					free(cat);
					return NULL;
				}
				cat = dynamem;
			}
			tempcat = strcat(tempcat, s);
		}
		// TODO optimize memory to size of string
		//cat = (char *) realloc(tempcat, strlen(tempcat) + 1);
		cat = tempcat;

		va_end(ap);
	}

	return cat;

}/*}}}*/

// close database
void
db_close(void) {
/*{{{*/

	sqlite3_close(db);

	return;

}/*}}}*/
