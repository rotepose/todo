# todo
> a simple todo list

*"Without a plan, there's no attack. Without attack, there's no victory." ~ Curtis Armstrong.* 

## Usage

**todo** will allow you to enter a task on the command line when passed with the -a tag. When you are done with the task just mark it complete when passed with the -c tag, and it will be removed from your task list. To remove the task from the database entirely, call todo with the -r tag. 

## Installation & Dependencies

**todo** has been built with sqlite3, and needs to be linked as such. Of course, the libraries must be installed to work, i.e.:

```shell
# pacman -S sqlite
```

Then clone and compile the repository:

```shell
$ git clone https://gitlab.com/rotepose23/todo.git
$ cd todo
$ make
```
## Version

0.011 - first usable version, see CHANGELOG for details.

## Links

- Project Homepage: https://gitlab.com/rotepose23/todo/
- Other Rotepose efforts:
  - a screensaver for terminal: https://gitlab.com/rotepose23/shmatrix/

## License

[BSD-3-Clause](https://opensource.org/licenses/BSD-3-Clause)
